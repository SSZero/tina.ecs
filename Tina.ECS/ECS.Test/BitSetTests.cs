﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TinaECS.ECS;

namespace ECS.Test
{
    [TestFixture]
    class BitSetTests
    {
        [Test]
        public void SetBitSetsBit()
        {
            var s = new BitSet(2);
            s.SetBit(5, true);
            Console.WriteLine("Test1 GO!");
            Assert.AreEqual(true, s.GetBit(5));
            Assert.AreEqual(false, s.GetBit(4));
            Assert.AreEqual(false, s.GetBit(6));
        }

        [Test]
        public void SetBitResizesArray()
        {
            var s = new BitSet(1);
            s.SetBit(77, true);
            Assert.AreEqual(true, s.GetBit(77));
            Assert.AreEqual(false, s.GetBit(76));
            Assert.AreEqual(false, s.GetBit(78));
        }

        [Test]
        public void UlongConstructorSetsBits()
        {
            var bits = (ulong) 1 << 13;
            var s = new BitSet(bits);
            
            Assert.AreEqual(true, s.GetBit(13));
        }

        [Test]
        public void BitSetCheckBitsTest()
        {
            var bs1 = new BitSet((ulong)1<<5);
            var bs2 = new BitSet(new ulong[2] {1 << 5, 1 << 3});
            var bs3 = new BitSet(0ul);

            Assert.IsTrue(bs2.CheckBits(bs1));
            Assert.IsFalse(bs2.CheckBits(bs3));
        }

        [Test]
        public void BitSetCopyConstructorCopiesBits()
        {
            ulong fourAndSix = (ulong)1 << 4 | 1 << 6;
            var bs1 = new BitSet(fourAndSix);

            var bs2 = new BitSet(bs1);

            Assert.AreEqual(fourAndSix, bs2.GetBits()[0]);
        }
    }
}
