﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NSubstitute;
using TinaECS.ECS;

namespace ECS.Test
{
    [TestFixture]
    class EntityWorldTests
    {
        [Test]
        public void EntityWorldCreateEntityAddsEntity()
        {
            EntityWorld world = new EntityWorld();

            Assert.IsEmpty(world.Entities);

            var entity = world.CreateEntity();

            Assert.IsNotEmpty(world.Entities);

            Assert.AreEqual(entity, world.Entities[0]);
        }

        [Test]
        public void EntityWorldCreateEntityWithComponentsAddsEntityWithComponents()
        {
            EntityWorld world = new EntityWorld();

            world.CreateEntity(new TestScoreComponent());

            Assert.IsNotEmpty(world.Entities);
            Assert.AreNotEqual(0, world.GetComponentBit<TestScoreComponent>());

        }

        [Test]
        public void EntityWorldCreateEntityWithProtoTypeCreatesEntity(){
            EntityWorld world = new EntityWorld();

            TestScoreEntityPrototype protoType = new TestScoreEntityPrototype(123);

            var entity = world.CreateEntity(protoType);
            var bit = world.GetComponentBit<TestScoreComponent>();
            
            var entityScore = entity.GetComponent<TestScoreComponent>().Score;

            Assert.AreNotEqual(0, bit);
            Assert.AreEqual(123, entityScore);
        }

        [Test]
        public void EntityWorldDeleteDeletesEntity()
        {
            EntityWorld world = new EntityWorld();

            // create 20 entities
            for (int i = 0; i < 20; i++)
            {
                world.CreateEntity();
            }

            foreach (var entity in world.Entities)
            {
                world.DeleteEntity(entity);
            }

            Assert.IsEmpty(world.Entities);

        }

        [Test]
        public void EntityWorldGetUnregisteredComponentRegistersBit()
        {
            var world = new EntityWorld();
            var bit = world.GetComponentBit<TestScoreComponent>();
            Assert.AreNotEqual(0, bit);
        }
    }
}
