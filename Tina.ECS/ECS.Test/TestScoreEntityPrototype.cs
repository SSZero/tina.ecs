﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinaECS.ECS;

namespace ECS.Test
{
    class TestScoreEntityPrototype : IEntityPrototype
    {
        long _initialScore = 0;
        public TestScoreEntityPrototype(long initialScore)
        {
            _initialScore = initialScore;
        }
        public Entity ConstructEntity(EntityWorld world)
        {
            var entity = world.CreateEntity();

            entity.AddComponent(new TestScoreComponent());
            entity.GetComponent<TestScoreComponent>().Score = _initialScore;

            return entity;
        }
    }
}
