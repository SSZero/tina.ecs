﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinaECS.ECS;

namespace ECS.Test
{
    class TestScoreComponent : IComponent
    {
        public long Score { get; set; }
    }
}
