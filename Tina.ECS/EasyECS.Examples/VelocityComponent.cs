﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using TinaECS.ECS;

namespace TinaECS.Examples
{
    class VelocityComponent : IComponent
    {
        public VelocityComponent(Vector2d vel)
        {
            Velocity = vel;
        }

        public Vector2d Velocity{ get; set;}

    }
}
