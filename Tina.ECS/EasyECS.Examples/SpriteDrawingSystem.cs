﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinaECS.ECS;
using TinaECS.Examples;

namespace EasyECS.Examples
{
    class SpriteDrawingSystem : DrawingSystem
    {
        public SpriteDrawingSystem(EntityWorld world) : base(world)
        {
        }

        public override void Process(Entity e)
        {
            throw new NotImplementedException();
        }

        protected override void SetFlags()
        {
            Flags.SetBit(World.GetComponentBit<PositionComponent>(), true);
            Flags.SetBit(World.GetComponentBit<SpriteComponent>(), true);
        }
    }
}
