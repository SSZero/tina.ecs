﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinaECS.ECS;

namespace TinaECS.Examples
{
    class VelocitySystem : ECS.System
    {
        
        public override void Process(Entity e)
        {
            // skip processing if the entity doesn't have the required components
            if(!e.HasComponents(Flags))
            {
                return;
            }

            var positionComponent = e.GetComponent<PositionComponent>();
            var velocityComponent = e.GetComponent<VelocityComponent>();

            positionComponent.Position += velocityComponent.Velocity;
            //Console.WriteLine("New position " + positionComponent.Position.ToString());
        }

        protected override void SetFlags()
        {
            Flags.SetBit(World.Bits[typeof(PositionComponent)], true);
            Flags.SetBit(World.Bits[typeof(VelocityComponent)], true);
        }

        public VelocitySystem(EntityWorld world) : base(world)
        {
        }
    }
}
