﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using TinaECS.ECS;

namespace TinaECS.Examples
{
    class PositionComponent : IComponent
    {

        public PositionComponent(Vector2d pos)
        {
            Position = pos;
        }

        public Vector2d Position { get; set; }
    }
}
