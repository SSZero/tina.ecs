﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using TinaECS.ECS;

namespace TinaECS.Examples
{
    class SpriteComponent : IComponent
    {
        public Bitmap Sprite { get; private set; }

        public SpriteComponent(Bitmap spriteData)
        {
            Sprite = spriteData;
        }

    }
}
