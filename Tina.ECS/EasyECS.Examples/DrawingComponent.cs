﻿using TinaECS.ECS;

namespace EasyECS.Examples
{
    public abstract class DrawingSystem : TinaECS.ECS.System
    {
        public void Draw(Entity e)
        {
            Process(e);
        }

        protected DrawingSystem(EntityWorld world)
            : base(world)
        {
        }
    }
}