﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using TinaECS.ECS;

namespace TinaECS.Examples
{
    class MovableEntityPrototype : IEntityPrototype
    {
        private Vector2d _initialVelocity;
        private Vector2d _initialPosition;

        public MovableEntityPrototype()
        {
            _initialVelocity = new Vector2d();
            _initialPosition = new Vector2d();
        }

        public MovableEntityPrototype(Vector2d initialPosition, Vector2d initialVelocity)
        {
            _initialVelocity = initialVelocity;
            _initialPosition = initialPosition;
        }

        public Entity ConstructEntity(EntityWorld world)
        {

            Entity e = world.CreateEntity(new PositionComponent(_initialPosition),
                                            new VelocityComponent(_initialVelocity));
            return e;
        }
    }
}
