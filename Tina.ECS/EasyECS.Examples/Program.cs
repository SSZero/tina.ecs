﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Linq;
using TinaECS.ECS;

namespace TinaECS.Examples
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {

            PerformanceTest();

        }

        private static void PerformanceTest()
        {
            
            EntityWorld world = new EntityWorld();
            List<Entity> entities = new List<Entity>();
            IEnumerator<Entity> entityEnumerator;
            List<ISystem> systems = new List<ISystem>();
            systems.Add(new VelocitySystem(world));

            var prototype = new MovableEntityPrototype(new Vector2d(1.0, 1.0), new Vector2d(0.0, 0.5));
            Stopwatch s = new Stopwatch();

            for (int trial = 1; trial < 1000000; trial *= 2)
            {

                for (var i = 0; i < trial; i++)
                {
                    //Entity e =
                        world.CreateEntity(prototype);
                    //entities.Add(e);
                }
                entities = world.Entities;
                var total = 0L;
                for (var i = 0; i < 10; i++)
                {
                    s.Start();
                    foreach (var entity in entities)
                    {
                        foreach (var system in systems)
                        {
                            system.Process(entity);    
                        }
                        world.DeleteEntity(entity);
                    }
                    s.Stop();
                    var result = s.ElapsedMilliseconds;
                    total += result;
                    Console.WriteLine("Round " + i + " Took: " + result + "ms");
                    s.Reset();
                }
                var average = total / 10.0;

                s.Reset();
                Console.WriteLine("Trial with " + trial + " entities took " + average + "ms");
                Console.ReadKey();
                entities.Clear();
            }



            List<Vector2d> aList = new List<Vector2d>();
            List<Vector2d> bList = new List<Vector2d>();

            for (var i = 0; i < 520000; i++)
            {

                Vector2d a = new Vector2d(10.0, 10.0);
                Vector2d b = new Vector2d(0.0, 1.0);

                aList.Add(a);
                bList.Add(b);

            }
            s.Reset();
            s.Start();

            for (int i = 0; i < 520000; i++)
            {
                aList[i] += bList[i];
            }
            s.Stop();
            var t = s.ElapsedMilliseconds;
            Console.WriteLine("520000 vector additions took: "+t+"ms");

            Console.ReadKey();
        }
    }
}
