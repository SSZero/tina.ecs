﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinaECS.ECS
{
    public sealed class BitSet
    {
        private ulong[] bits;

        public BitSet()
        {
            bits = new ulong[1];   
        }
        public BitSet(int size)
        {
            bits = new ulong[size]; 
        }
        /// <summary>
        /// Creates a new <see cref="BitSet"/> copying the bits from the source <see cref="BitSet"/>
        /// </summary>
        /// <param name="copyFrom"></param>
        public BitSet(BitSet source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            var size = source.bits.Count();
            var temp = new ulong[size];
            Array.Copy(source.bits, temp, size);
            bits = temp;
        }

        public BitSet(int[] indexes)
        {
            bits = new ulong[1];
            foreach (var index in indexes)
            {
                SetBit(index, true);
            }
        }

        public BitSet(ulong flags)
        {
            bits = new ulong[1]{flags};
        }

        public BitSet(ulong[] bits)
        {
            this.bits = bits;
        }
        public bool GetBit(int index)
        {
            var arrayIndex = index/64;
            var bitIndex = index%64;
            if (arrayIndex < bits.Length)
            {
                return ((bits[arrayIndex] & (1ul << bitIndex)) > 0);
            }
            return false;
        }
        public ulong[] GetBits()
        {
            var copy = new ulong[bits.Count()];
            Array.Copy(bits, copy, bits.Length);
            return copy;
        }
        public void SetBit(int index, bool val)
        {
            var arrayIndex = index/64;
            var bitIndex = index%64;
            if (arrayIndex >= bits.Length)
            {
                resizeBits(arrayIndex+1);
            }

            if (val) {
                bits[arrayIndex] |= (1ul << bitIndex);
            }
            else
            {
                bits[arrayIndex] &= ~(1ul << bitIndex);
            }
            
        }

        public bool CheckBits(BitSet bitSet)
        {
            int length = Math.Min(this.bits.Length, bitSet.bits.Length);
            for (var i = 0; i < length; i++)
            {
                if ((bits[i] & bitSet.bits[i]) == 0)
                {
                    return false;
                }
            }
            return true;
        }
        private void resizeBits(int length)
        {
            var temp = new ulong[length];
            Array.Copy(bits, temp, bits.Length);
            bits = temp;
        }
    }
}
