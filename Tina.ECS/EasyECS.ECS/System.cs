﻿namespace TinaECS.ECS
{
    public abstract class System : ISystem
    {
        protected BitSet Flags { get; private set; }
        protected EntityWorld World { get; set; }

        public abstract void Process(Entity e);
        protected abstract void SetFlags();

        public System(EntityWorld world)
        {
            World = world;
            Flags = new BitSet(1);
            SetFlags();
        }

        

        public BitSet GetFlags()
        {
            return Flags;
        }
    }
}