﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace TinaECS.ECS
{
    public sealed class Entity
    {
        private Dictionary<int, IComponent> Components;
        private Dictionary<Type, int> ComponentBits;

        private EntityWorld world;

        //private List<Component> Components;
        public BitSet flags = new BitSet(1);


        internal Entity(EntityWorld world)
        {
            this.world = world;
            Components = new Dictionary<int, IComponent>();
            ComponentBits = new Dictionary<Type, int>();
        }

        internal Entity(EntityWorld world, params IComponent[] components)
            : this(world)
        {

            Components = new Dictionary<int, IComponent>(components.Length);
            ComponentBits = new Dictionary<Type, int>(components.Length);
            foreach(var component in components){
                AddComponent(component);
            }
        }

        public bool HasComponents(BitSet flags)
        {
            return this.flags.CheckBits(flags);
        }

        public bool HasComponent(int index)
        {
            return flags.GetBit(index);
        }
        public bool HasComponent<T>() where T : IComponent
        {
            var bit = world.GetComponentBit<T>();
            return HasComponent(bit);
        }

        public T GetComponent<T>() where T : IComponent
        {
            var component = (T)Components[world.Bits[typeof(T)]];
            return component;

        }
        public void AddComponent(IComponent c)
        {
            var index = world.GetBit(c);
            if (!HasComponent(index))
            {
                Components.Add(index, c);
                flags.SetBit(index, true);
            }
        }

    }
}
