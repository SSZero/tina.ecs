﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinaECS.ECS
{
    public class ComponentType
    {
        public Type Type { get; private set; }
        public int Bit { get; private set; }

        public ComponentType(Type type, int bit)
        {
            this.Type = type;
            this.Bit = bit;
        }

        public override bool Equals(object obj)
        {
            if(obj is ComponentType)
            {
                var comp = obj as ComponentType;
                return comp.Bit == this.Bit && comp.Type == this.Type;
            }
            return false;
        }
    }
}
