﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;


namespace TinaECS.ECS
{
    public sealed class EntityWorld : IEntityWorld
    {
       // private static EntityWorld _instance;

        public BitRegistry Bits;

        private HashSet<Entity> _entities;  
        
        private static readonly object _exclusiveLock = new object();

        private int _currentBit = 1;

        private readonly Dictionary<Type, int> _componentBits;
        private readonly Dictionary<int, Type> _componentTypes; 

        public EntityWorld()
        {
            _componentBits = new Dictionary<Type, int>();
            _entities = new HashSet<Entity>();
            Bits = new BitRegistry();
        }

        public List<Entity> Entities
        {
            get { return new HashSet<Entity>(_entities).ToList(); }
        }


/*
        public static EntityWorld Instance()
        {
            lock (_exclusiveLock)
            {
                return _instance ?? (_instance = new EntityWorld());
            }
        }
*/

        public Entity CreateEntity()
        {
            Entity e = new Entity(this);
            _entities.Add(e);
            return e;
        }

        public Entity CreateEntity(params IComponent[] components)
        {
            Entity e = new Entity(this, components);
            _entities.Add(e);
            return e;
        }

        public Entity CreateEntity(IEntityPrototype entityPrototype)
        {
            Entity e = entityPrototype.ConstructEntity(this);
            _entities.Add(e);
            return e;
        }

        public void DeleteEntity(Entity e)
        {
            _entities.Remove(e);
        }

        

        public int GetBit(IComponent c)
        {
            return Bits[c.GetType()];
        }

        public int GetComponentBit<T>() where T : IComponent
        {
            return Bits[typeof(T)];
        }


        private int GetNextBit()
        {
            return _currentBit++;
        }

    }
}
