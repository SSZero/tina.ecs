﻿using System.Collections.Generic;

namespace TinaECS.ECS
{
    public interface IEntityPrototype
    {
        Entity ConstructEntity(EntityWorld world);
    }
}