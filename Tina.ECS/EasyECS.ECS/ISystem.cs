﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinaECS.ECS
{
    public interface ISystem
    {
         void Process(Entity e);
         BitSet GetFlags();
    }
}
