﻿using System;
using System.Collections.Generic;

namespace TinaECS.ECS
{
    public sealed class BitRegistry
    {
        private int _currentBit;
        private readonly Dictionary<Type, int> _componentBits;

        public BitRegistry()
        {
            _componentBits = new Dictionary<Type, int>();
            _currentBit = 1;
        }

        public int RegisterComponent(Type componentType)
        {
            var result = _currentBit++;
            _componentBits.Add(componentType, result);
            return result;
        }

        public int this[Type t]
        {
            get
            {
                if (_componentBits.ContainsKey(t))
                {
                    return _componentBits[t];
                }
                else if (typeof(IComponent).IsAssignableFrom(t))
                {
                    return RegisterComponent(t);
                }
                else
                {
                    throw new ArgumentException("Type "+t+" does not extend IComponent");
                }
                
            }
        }


    }
}
