﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinaECS.ECS
{
    interface IEntityWorld
    {
        List<Entity> Entities{ get; }

        Entity CreateEntity();
        Entity CreateEntity(params IComponent[] components);
        Entity CreateEntity(IEntityPrototype entityPrototype);
        void DeleteEntity(Entity e);
        int GetComponentBit<T>() where T : IComponent;
    }
}
